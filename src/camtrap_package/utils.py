import collections
from datetime import datetime, timezone
import json
import requests

from camtrap_package.templates import CAMTRAP_PACKAGE_RESOURCES


def get_timestamp():
    local_time = datetime.now(timezone.utc).astimezone()
    return local_time.isoformat()


def check_url(url):
    headers = requests.head(url)
    return headers


def check_species_name(name):
    # species name validation with GBIF fuzzy match
    # http://api.gbif.org/v1/species/match?name=cervus%20elaphu
    pass


def deep_merge(d, u, merge_lists=False):
    """
    Do a deep merge of one dict into another.
    This will update d with values in u, but will not delete keys in d
    not found in u at some arbitrary depth of d. That is, u is deeply
    merged into d.

    Args -
      d, u: dicts

    Note: this is destructive to d, but not u.
    Returns: None
    """
    stack = [(d, u)]
    while stack:
        d, u = stack.pop(0)
        for k, v in u.items():
            if not isinstance(v, collections.Mapping):
                # check if u[k] is a list, if so extend d[k]
                if merge_lists and isinstance(v, list):
                    dv = d.setdefault(k, [])
                    new_list = dv + v
                    try:
                        d[k] = list(set(new_list))
                    except TypeError:
                        # the try block throws TypeError when processing set() on list of dicts
                        # as dicts are not hashable
                        # the solution is to temporarily turn them back to str and run set()
                        new_list = [json.dumps(x, sort_keys=True) for x in new_list]
                        new_list = list(set(new_list))
                        new_list = [json.loads(x) for x in new_list]
                        d[k] = new_list
                else:
                    # u[k] is neither dict nor list, nothing to merge, so
                    #  just set it, regardless if d[k] *was* a dict
                    d[k] = v
            else:
                # note: u[k] is a dict
                # get d[k], defaulting to a dict, if it doesn't previously
                # exist
                dv = d.setdefault(k, {})
                if not isinstance(dv, collections.Mapping):
                    # d[k] is not a dict, so just set it to u[k],
                    # overriding whatever it was
                    d[k] = v
                else:
                    # both d[k] and u[k] are dicts, push them on the stack
                    # to merge
                    stack.append((dv, v))


def filter_list_of_dicts(_list, key, values):
    return list(filter(lambda d: d.get(key, "") in values, _list))


def generate_profile_template(schema_url, schema_version, return_required=False):
    """
    Generates an empty template of Camtrap DP profile based on the full
    (merged) schema.
    """
    print(schema_url)
    schema = json.loads(requests.get(schema_url).content)
    schema_base = json.loads(requests.get(schema["allOf"][0]["$ref"]).content)
    # merge both schemas
    deep_merge(schema_base, schema["allOf"][1], merge_lists=True)

    default_values = {
        "string": "",
        "int": 0,
        "integer": 0,
        "number": 0,
        "array": [],
        "list": [],
        "tuple": [],
        "dict": {},
        "boolean": "false",
    }

    template = {}

    def walk_schema(schema, parent=None):
        for k, v in schema.items():
            if k == "properties":
                for k2, v2 in v.items():
                    v2_type = v2.get("type", "string")
                    # if > 2 types just take the first one
                    if type(v2_type) is list:
                        v2_type = v2_type[0]
                    if v2_type == "object":
                        if not parent:
                            template[k2] = {}
                            parent = template[k2]
                        else:
                            parent[k2] = {}
                            parent_prev = parent
                            parent = parent[k2]
                        walk_schema(v2, parent=parent)
                        parent = parent_prev
                    elif v2_type == "array" and v2.get("items", {}).get("properties"):
                        if not parent:
                            template[k2] = [{}]
                            parent = template[k2][0]
                        else:
                            parent[k2] = [{}]
                            parent_prev = parent
                            parent = parent[k2][0]
                        walk_schema(v2["items"], parent=parent)
                        parent = parent_prev
                    else:
                        if parent is None:
                            parent = template
                        parent[k2] = default_values[v2_type]
            else:
                continue

    walk_schema(schema_base)

    # update resources definitions
    del template["resources"]
    template["resources"] = CAMTRAP_PACKAGE_RESOURCES

    if return_required:
        return (template, schema_base["required"])
    return template
