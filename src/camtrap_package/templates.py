"""
Templates
"""

CAMTRAP_PACKAGE_RESOURCES = [
    {
        "name": "deployments",
        "path": "deployments.csv",
        "profile": "tabular-data-resource",
        "format": "csv",
        "mediatype": "text/csv",
        "encoding": "utf-8",
        "schema": "",
    },
    {
        "name": "media",
        "path": "media.csv",
        "profile": "tabular-data-resource",
        "format": "csv",
        "mediatype": "text/csv",
        "encoding": "utf-8",
        "schema": "",
    },
    {
        "name": "observations",
        "path": "observations.csv",
        "profile": "tabular-data-resource",
        "format": "csv",
        "mediatype": "text/csv",
        "encoding": "utf-8",
        "schema": "",
    },
]

HTML_REPORT = """
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Frictionless Components report</title>
    <link rel="stylesheet" href="https://unpkg.com/frictionless-components/dist/frictionless-components.css">
    <script type="module" src="https://unpkg.com/frictionless-components/dist/frictionless-components.js"></script>
  </head>
  <body>
    <div id="report"></div>
    <script type="module">
      const report = $report;
      const element = document.getElementById("report");
      const frictionlessComponents = window.frictionlessComponents;
      frictionlessComponents.render(frictionlessComponents.Report, {report}, element);
    </script>
  </body>
</html>
"""
